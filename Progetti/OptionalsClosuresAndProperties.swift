// Simone Tugnetti

// Optionals, Closures and Properties

var str: String = "Hello"

// Variabile nullable avente un valore
var maybeStr: String? = "Hi"

// Variabile nullable avente nessun valore o nullo
var nilStr: String? = nil

// Unwrapping -> Metodo in cui viene prelevato il valore da una 
// variabile nullable

// Unwrapping forzato -> la variabile deve necessariamente essere non nil
print(maybeStr!.count)

// If let Unwrapping -> assegna il valore della variabile nullable ad una costante
// nel caso in cui vi sia un valore al suo interno, utilizzabile solo all'interno dell'if
if let unwrappedStr = maybeStr {
    print("\(unwrappedStr) -> Exist")
} else {
    print("Variable is nil")
}

// Guard Unwrapping -> assegna il valore della variabile nullable ad una costante
// nel caso in cui vi sia un valore al suo interno, utilizzabile solo all'interno
// della funzione, altrimenti la stessa verrà bloccata e terminata 
func getString(_ str: String?) {
    guard let unwrapped = str else {
        print("Guard -> Variable is nil")
        return
    }
    print("\(unwrapped) -> Exist in Func")
}

getString(nilStr)

print()

// Closures -> sintetizzare funzioni, classi, ecc... all'interno di variabili

func saySomething(_ spell: String) -> String {
    return spell
}

// Closure con stessa funzionalità della funzione precedente
var newSay = { (spell: String) -> String in
    return spell
}

print(saySomething("Ciao"))
print(newSay("Nuovo Ciao"))

print()

// Struct -> Struttura di dati value type, è possibile cioè prelevarne e modificarne i dati
// senza necessariamante avere un riferimento di indirizzo
struct Animal {
    var name: String = ""
    var heightInches = 0.0

    // E' possibile settare e prelevare il valore di una variabile utilizzando 
    // le variabili all'interno della struct
    var heightCM: Double {
        get {
            return 2.54 * heightInches
        }
        set (newHeightCM) {
            heightInches = newHeightCM / 2.54
        }
    }
}

// Class -> Struttura di dati reference type, è possibile cioè prelevarne e modificarne i dati
// utilizzando il riferimento di indirizzo alla classe specifica
class Number {
    var n: Int
    init(n: Int) {
        self.n = n
    }
}

let dog = Animal(name: "dog", heightInches: 50)

var num = Number(n:53)
var newNum = num

print("\(dog.heightInches) and \(dog.heightCM)")
print(num.n)
newNum.n = 7

// I valori saranno identici
print(newNum.n)
print(num.n)