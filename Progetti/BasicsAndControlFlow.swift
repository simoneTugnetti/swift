// Simone Tugnetti

// Swift Basics and Control Flow

print("Hello world")

// Variabile con tipo implicito String
var str = "Hello world Variable"

// Variablie con tipo esplicito String
// Optional -> la variabile può contenere al suo interno valore nullo
var str2: String? = nil

str2 = "Hey"

// Costante -> il valore non può essere modificato
let constString = "Hello"

print(str)


// Blocco If -> In base alla condizione, se True o False,
// verrà scelto un ramo da seguire

let name = "Johnathan"

if name.count > 10 {
    print("Long Name")
} else if name.count > 5 {
    print("Medium Name")
} else {
    print("Short Name")
}

// Range -> identifica un rage di valori da un valore incluso (...)
// od escluso (..<)

// Switch -> In base al valore, verrà scelto un case da seguire,
// nel caso non vi sia un case specifico, verrà scelto default
switch name.count {
case 10:
    print("10 char name")
case 8...10:
    print("between 8 and 10 char name")
case 4..<8:
    print("between 4 and 8 excluse char name")
default:
    print("minus of 3 char name")
}

var numb = 0

// Ciclo While -> Finchè la condizione è vera, verrà eseguito il blocco
// di codice al suo interno
while numb < 10 {
    print("numb: ", numb)
    numb = numb + 1
}

// Ciclo For -> blocco di codice ripetuto per n volte in base al range inserito 
for number in 0..<10 {
    print("For number: ", number)
}

// E' possibile eseguire un ciclo di valori all'interno di un array
for number in [2,5,1,9,6] {
    print("For array: ", number)   
}