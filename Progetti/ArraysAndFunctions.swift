// Simone Tugnetti

// Array, Dictionaries And Functions

// Array -> Lista di valori
// Array di tipo implicito String
var animals = ["Cow", "Dog", "Bunny"]

// Array di tipo esplicito String
let animalsT: [String]

// Create an empty array of Int
var emptyAnim = [Int]()

print(animals[1])

animals[2] = "Rabbit"

// Cicla ogni elemento all'interno dell'array
for animal in animals {
    print(animal)
}

// Dictionary -> Una collezione di informazioni organizzate come "chaive - valore"
var food = ["Cow": "Grass", "Dog": "Bone", "Cat": "Tuna"]

// La variabile sarà automaticamente di tipo optional
let dogEat = food["Dog"]

// Dato che è nullabile, deve essere forzata la lettura del contenuto NON nullo
print("Dog eat \(food["Dog"]!)")

print()

// Cicando un dictionary, verrà restituito sia chiave che valore all'interno 
// di una specifica posizione
for eat in food {
    print("\(eat.key)")
    print("\(eat.value)")
}

// Funzioni

// Funzione base
func firstFun() {
    print("Doing something")
}

// Funzione con parametri
// I parametri possono essere esplicitati sia all'esterno che all'interno della funzione
// assegnandogli delle labels.
// La prima è ad uso esterno ("_" se non si volesse utilizzare) e la seconda ad uso interno
func animFood(_ animal:String, _ food:String) {
    print("\(animal) eat \(food)")
}

// Funzione con ritorno
// Utilizzando un arrow, è possibile identificare il tipo di valore che la funzione può ritornare
func perform(_ operation:String, on a:Double, and b:Double) -> Double{
    print("Performing \(operation) on \(a) and \(b)")
    switch operation {
    case "+":
        return a+b
    case "-":
        return a-b
    case "*":
        return a*b
    case "/":
        return a/b
    default:
        return 0
    }
}


firstFun()
animFood("Rabbit", "carrot")

let result = perform("+", on:2.0, and:1.0)
print("Il risultato e' \(result)")

print()

// Array bidimensionale o matrice
var someValue = [
    [5, 4, 10],
    [7, 1, 9],
    [6, 3, 2]
]

// Per identificare una matrice si utilizza il tipo [[Int]]
func printValue(_ values: [[Int]]) {
    for row in 0..<values.count {
        for col in 0..<values[row].count {
            print(values[row][col])
        }
    }
}

// Normalemnte un parametro non modifica direttamente il dato passato dall'esterno,
// utilizzando però la dicitura "inout" è possibile modificare il dato in questione
// dall'interno della funzione 
func changeValue(_ values: inout[[Int]]) {
    for row in 0..<values.count {
        for col in 0..<values[row].count {
            if values[row][col] > 5 {
                values[row][col] = 5
            }
        }
    }
}

printValue(someValue)

// Viene passato l'indirizzo appartenente al dato da passare
changeValue(&someValue)
print()
printValue(someValue)