// Simone Tugnetti

// Protocols, Extensions and Enum

// Class -> Struttura di dati reference type, è possibile cioè prelevarne e modificarne i dati
// utilizzando il riferimento di indirizzo alla classe specifica
class Number {
    var n: Int
    init(n: Int) {
        self.n = n
    }
}

var num = Number(n:53)
var newNum = num

print(num.n)
newNum.n = 7
print(newNum.n)

// Extension -> Utilizzato per accrescere librerie, classi, struct, ecc... con funzioni
// o proprietà aggiuntive

// Estensione di un tipo
extension Int {
    func getRandomValue() -> Int {
        return 6
    }
}

// Estensione di una classe
extension Number {
    func printHello() {
        print("Hello")
    }
}

print(num.n.getRandomValue())
num.printHello()

// Protocols -> Tipo utilizzato per implementare da altre strutture dati
// i metodi al suo interno
protocol dancable {
    func dance() -> Bool
}

// La classe Person deve implementare i metodi che eredita dal
// protocollo dancable
class Person: dancable {
    func dance() -> Bool {
        return true
    }
}

// Enum -> Struttura che permette di salvare ed utilizzare una lista
// di dati prefissta
enum TypesOfVeggies: String {
    case Carrots
    case Tomatoes
    case Celery
}

let carrot = TypesOfVeggies.Carrots

func eatVeggies(_ veggie: TypesOfVeggies) {
    print(veggie)
}

let randomVeggie = TypesOfVeggies(rawValue: "Carrots")
eatVeggies(TypesOfVeggies.Carrots)

// Classe base
class Car {
    var cupHolder: String
    
    // Costruttore richiesto al momento dell'istanza,
    // richiede un parametro
    required init(cupHolder: String) {
        self.cupHolder = cupHolder
    }

    // Costruttore convenzionale, richiamato nel caso non venga
    // fatto riferimento al costruttore necessario
    convenience init() {
        self.init(cupHolder: "Cool")
    }
    
    // Distruttore della classe, richiamato al momento dell'eliminazione
    // dell'istanza
    deinit {
        cupHolder = ""   
    }
    
}

// Required constructor
let car = Car(cupHolder: "Cool")

// Convenience constructor
let newCar = Car()